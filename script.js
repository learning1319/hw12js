//Події клавіатури не завжди повністю відповідатимуть введеному в input. 
//Якщо використати копіпаст, то події клавіатури не спрацюють, а поле буде заповнене.

window.addEventListener('keydown', (elem) => {
    const key = elem.key.toUpperCase();
    const buttons = document.querySelectorAll('.btn')

    buttons.forEach(function(button){
        button.style.backgroundColor = '#000000';
        console.log(button.textContent)
        if (button.textContent.toUpperCase() === key) {
            button.style.backgroundColor = '#0000FF';
        }
    });


}
)